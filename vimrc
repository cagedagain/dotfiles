" :so $MYVIMRC - reload vimrc
"
" Install pathogen
" mkdir -p ~/.vim/autoload ~/.vim/bundle && \
" > curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
execute pathogen#infect()

colorscheme elflord

if has('gui_running')
	colorscheme twilight
endif

set nocompatible              " be iMproved, required
filetype off                  " required
let mapleader = ","
syntax on

"Remap '0' to the first non-blank character
map 0 ^

"Don't yank deleted lines
"nnoremap <leader>x "_x
"nnoremap <leader>d "_d
"vnoremap <leader>dd "_dd
"vnoremap <leader>p "_dP

"Set tab and spacing configuration
"set expandtab!
"set smartindent
set tabstop=4
set shiftwidth=4
set list lcs=tab:▸.

"START VUNDLE CONFIGURATION

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

"Plugins
Plugin 'scrooloose/nerdtree'
Plugin 'kien/ctrlp.vim'
Plugin 'bling/vim-airline'
Plugin 'jiangmiao/auto-pairs'
Plugin 'tpope/vim-fugitive'
Plugin 'scrooloose/nerdcommenter'
Plugin 'AutoComplPop'
Plugin 'vim-scripts/taglist.vim'
Plugin 'terryma/vim-multiple-cursors'
Plugin 'rking/ag.vim'
Plugin 'groenewege/vim-less'
Plugin 'moll/vim-bbye'
Plugin 'christoomey/vim-tmux-navigator'
Plugin 'rizzatti/dash.vim'
Plugin 'pbrisbin/vim-mkdir'
Plugin 'mtth/scratch.vim'
Plugin 'danro/rename.vim'
Plugin 'd11wtq/ctrlp_bdelete.vim'
Plugin 'TaskList.vim'
Plugin 'kien/rainbow_parentheses.vim'
Plugin 'pangloss/vim-javascript'
Plugin 'mxw/vim-jsx'
Plugin 'sbdchd/neoformat'
Plugin 'mhinz/vim-startify'
Plugin 'jelera/vim-javascript-syntax'

"Clojure plugins
"Plugin 'tpope/vim-salve'
"Plugin 'tpope/vim-fireplace'
"Plugin 'guns/vim-clojure-static'
"Plugin 'tpope/vim-classpath'

"Ultisnips and dependencies
Plugin 'SirVer/ultisnips'
Plugin 'honza/vim-snippets'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

"END VUNDLE CONFIGURATION

"Enable airline buffer list
let g:airline#extensions#tabline#enabled = 1
"Only show filename in buffer list (not full path)
let g:airline#extensions#tabline#fnamemod = ':t'
"Hide whitespace and indent counts
let g:airline#extensions#whitespace#enabled = 0
"Show powerline symbols

"Return to last edit position when opening files (You want this!)
autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif
"Remember info about open buffers on close
set viminfo^=%

"Bug fix for airline status bar only showing in split windows
set laststatus=2

"Map close buffer
nnoremap <leader>w :Bdelete<CR>

" Init Ctrlp Buffer delete function (c-2)
call ctrlp_bdelete#init()

"Map open filetype
nnoremap <leader>b :CtrlPBuffer<cr>
nnoremap <leader>p :CtrlP<cr>

"Allow cltrp to show more results in window
let g:ctrlp_match_window = 'results:10'

"Ignore
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*/publish/public/*,*/publish/vendor/*,*/documentation/*,*/node_modules/*

"Grep search
nnoremap <leader>g :silent execute "Ag! -n -R " . expand("<cword>") . " ."<cr>:copen<cr>
vnoremap <leader>g :silent execute "Ag! -n -R " . expand("<cword>") . " ."<cr>:copen<cr>

"Show and close quickfix window
map <leader>sr :copen<CR>
map <leader>cr :cclose<CR>

"Change current window
map <leader><Tab> <C-w>w

"Custom Grep command
function! s:MyAgFunction(searchParam)
	execute "silent Ag! -n -R '" . a:searchParam . "' ."
	:copen
endfunction
command! -nargs=1 MyAg call s:MyAgFunction(<f-args>)

"Shortcut Mygrep function
nnoremap <C-F> :MyAg

function! s:SyntaxItem()
	echo synIDattr(synID(line("."),col("."),1),"name")
endfunction
command! SyntaxItem call s:SyntaxItem()

function! s:ValidatePHP()
	execute "!php -l %"
endfunction
command! ValidatePHP call s:ValidatePHP()

"Customise taglist.vim
let Tlist_Show_One_File = 1
let Tlist_Use_Right_Window = 1
let Tlist_WinWidth = 36
let Tlist_GainFocus_On_ToggleOpen = 1
let Tlist_Highlight_Tag_On_BufEnter = 1
let tlist_php_settings='php;f:functions'
map <leader>fl :TlistToggle<CR>

"Customise TaskList.vim
map <unique> <leader>tl <Plug>TaskList

"Customise Ultisnips
"Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-tab>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"
"If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

"Disable git-gutter - enabled for gvim only
let g:gitgutter_enabled = 0
"Set symbols to pipes
let g:gitgutter_sign_added = '|'
let g:gitgutter_sign_modified = '|'
let g:gitgutter_sign_removed = '|'
let g:gitgutter_sign_removed_first_line = '|'
let g:gitgutter_sign_modified_removed = '|'

" Scratch settings
let g:scratch_autohide = &hidden
let g:scratch_insert_autohide = 1

" JSX Syntax highlighting and indentation within both.jsx and .js files
let g:jsx_ext_required = 0

"Trigger Prettier on save
let g:prettier#autoformat = 0
autocmd BufWritePre *.js,*.jsx,*.mjs,*.ts,*.tsx,*.css,*.less,*.scss,*.json,*.graphql,*.md,*.vue,*.yaml,*.html PrettierAsync
