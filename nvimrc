
" :so $MYVIMRC - reload vimrc
"
" Install pathogen
" mkdir -p ~/.vim/autoload ~/.vim/bundle && \
" > curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
call pathogen#infect()

colorscheme elflord

if has('gui_running') 
	colorscheme twilight
endif

set nocompatible              " be iMproved, required
filetype off                  " required
let mapleader = ","
syntax on

"Remap '0' to the first non-blank character
map 0 ^

"Don't yank deleted lines
"nnoremap <leader>x "_x
"nnoremap <leader>d "_d
"vnoremap <leader>dd "_dd
"vnoremap <leader>p "_dP

"Set tab and spacing configuration
"set expandtab!
"set smartindent
set tabstop=4
set shiftwidth=4
set list lcs=tab:▸. 

"START VUNDLE CONFIGURATION

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

"Plugins
Plugin 'scrooloose/nerdtree'
Plugin 'kien/ctrlp.vim'
Plugin 'bling/vim-airline'
Plugin 'jiangmiao/auto-pairs'
Plugin 'tpope/vim-fugitive'
Plugin 'scrooloose/nerdcommenter'
Plugin 'AutoComplPop'
Plugin 'vim-scripts/taglist.vim'
Plugin 'terryma/vim-multiple-cursors'
Plugin 'rking/ag.vim'
Plugin 'groenewege/vim-less'
Plugin 'moll/vim-bbye'
Plugin 'christoomey/vim-tmux-navigator'
Plugin 'rizzatti/dash.vim'
Plugin 'jelera/vim-javascript-syntax'
Plugin 'pbrisbin/vim-mkdir'
Plugin 'mtth/scratch.vim'
Plugin 'danro/rename.vim'
Plugin 'd11wtq/ctrlp_bdelete.vim'
Plugin 'TaskList.vim'

"Clojure plugins
Plugin 'tpope/vim-salve'
Plugin 'tpope/vim-fireplace'
Plugin 'guns/vim-clojure-static'
"Plugin 'tpope/vim-classpath'

"Ultisnips and dependencies
Plugin 'SirVer/ultisnips'
Plugin 'honza/vim-snippets'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

"END VUNDLE CONFIGURATION

"Enable airline buffer list
let g:airline#extensions#tabline#enabled = 1
"Only show filename in buffer list (not full path)
let g:airline#extensions#tabline#fnamemod = ':t'
"Hide whitespace and indent counts
let g:airline#extensions#whitespace#enabled = 0
"Show powerline symbols

"Return to last edit position when opening files (You want this!)
autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif
"Remember info about open buffers on close
set viminfo^=%

"Bug fix for airline status bar only showing in split windows
set laststatus=2

"Map close buffer
nnoremap <leader>w :Bdelete<CR>

" Init Ctrlp Buffer delete function (c-2)
call ctrlp_bdelete#init()

"Map open filetype
nnoremap <leader>b :CtrlPBuffer<cr>
nnoremap <leader>p :CtrlP<cr>

"Allow cltrp to show more results in window
let g:ctrlp_match_window = 'results:10'

"Ignore
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*/publish/public/*,*/documentation/*

"Grep search
nnoremap <leader>g :silent execute "Ag! -n -R " . expand("<cword>") . " ."<cr>:copen<cr>
vnoremap <leader>g :silent execute "Ag! -n -R " . expand("<cword>") . " ."<cr>:copen<cr>

"Show and close quickfix window
map <leader>sr :copen<CR>
map <leader>cr :cclose<CR>

"Change current window
map <leader><Tab> <C-w>w

"Custom Grep command
function! s:MyAgFunction(searchParam)
	execute "silent Ag! -n -R '" . a:searchParam . "' ."
	:copen
endfunction
command! -nargs=1 MyAg call s:MyAgFunction(<f-args>)

"Shortcut Mygrep function
nnoremap <C-F> :MyAg 

function! s:SyntaxItem()
	echo synIDattr(synID(line("."),col("."),1),"name")
endfunction
command! SyntaxItem call s:SyntaxItem()

function! s:ValidatePHP()
	execute "!php -l %"
endfunction
command! ValidatePHP call s:ValidatePHP()

"Customise taglist.vim
let Tlist_Show_One_File = 1
let Tlist_Use_Right_Window = 1
let Tlist_WinWidth = 36
let Tlist_GainFocus_On_ToggleOpen = 1
let Tlist_Highlight_Tag_On_BufEnter = 1
let tlist_php_settings='php;f:functions'
map <leader>fl :TlistToggle<CR>

"Customise Ultisnips
"Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-tab>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"
"If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

"Disable git-gutter - enabled for gvim only
let g:gitgutter_enabled = 0
"Set symbols to pipes
let g:gitgutter_sign_added = '|'
let g:gitgutter_sign_modified = '|'
let g:gitgutter_sign_removed = '|'
let g:gitgutter_sign_removed_first_line = '|'
let g:gitgutter_sign_modified_removed = '|'

" Scratch settings
let g:scratch_autohide = &hidden
let g:scratch_insert_autohide = 1



""""""""""""""""""" GVIMRC

"Set colorscheme and font
colorscheme twilight
set guifont=Monaco:h10

"Set window configuration
set guioptions-=r
set guioptions-=L
"set transparency=5
set nowrap
set nu "line numbers

"Disable bell sounds
set vb

"Autoload NERDTree with bookmarks open
let NERDTreeShowBookmarks=1
nnoremap <leader>nt :NERDTreeToggle<cr>

"Highlight search results and clear on ESC
set hlsearch
set incsearch
nnoremap <silent> <esc> :noh<cr><esc>

"Map move line up/down to Alt+up/down
"autocmd needed for mapping Alt key in MacVim
autocmd VimEnter * nnoremap <A-up> :m .-2<CR>
autocmd VimEnter * nnoremap <A-down> :m .+1<CR>
autocmd VimEnter * vnoremap <A-up> :m '<-2<CR>gv=gv
autocmd VimEnter * vnoremap <A-down> :m '>+1<CR>gv=gv

"Map change buffer
autocmd VimEnter * nnoremap <A-left> :bprevious!<CR>
autocmd VimEnter * nnoremap <A-right> :bnext!<CR>

"Set GitGutter colours
let g:gitgutter_enabled = 1
let g:gitgutter_sign_column_always = 1
"hi GitGutterDelete guibg=#FF0F0F guifg=#FF0F0F
"hi GitGutterChange guibg=#FAEA41 guifg=#FAEA41
"hi GitGutterChangeDelete guibg=#FAEA41 guifg=#FAEA41
"hi GitGutterAdd guibg=#6AD945 guifg=#6AD945
"Toggle between GitGutter and Line Numbers
"map <leader>l :GitGutterToggle<CR>

"Build project
nnoremap <leader>rr :! ~/Development/Akkroo/rebuildAll.sh<cr>
nnoremap <leader>ra :! ~/Development/Akkroo/rebuildApp.sh<cr>
nnoremap <leader>rn :! ~/Development/Akkroo/rebuildNativeApp.sh<cr>
nnoremap <leader>rb :! ~/Development/Akkroo/rebuildBackend.sh<cr>

"Tmux key binding
let g:tmux_navigator_no_mappings = 1
nnoremap <silent> <S-left> :TmuxNavigateLeft<cr>
nnoremap <silent> <S-down> :TmuxNavigateDown<cr>
nnoremap <silent> <S-up> :TmuxNavigateUp<cr>
nnoremap <silent> <S-right> :TmuxNavigateRight<cr>

" Centralize backups, swapfiles and undo history
set backupdir=/Users/ianedwards/.vim/backups
set directory=/Users/ianedwards/.vim/swaps
set undodir=/Users/ianedwards/.vim/undo
