"Set colorscheme and font
colorscheme twilight
set guifont=Monaco:h11

"Set window configuration
set guioptions-=r
set guioptions-=L
"set transparency=5
set nowrap
set nu "line numbers

"Disable bell sounds
set vb

"Autoload NERDTree with bookmarks open
let NERDTreeShowBookmarks=1
nnoremap <leader>nt :NERDTreeToggle<cr>
nnoremap <leader>nf :NERDTreeFind<cr>

"Highlight search results and clear on ESC
set hlsearch
set incsearch
nnoremap <silent> <esc> :noh<cr><esc>

"Map move line up/down to Alt+up/down
"autocmd needed for mapping Alt key in MacVim
autocmd VimEnter * nnoremap <A-up> :m .-2<CR>
autocmd VimEnter * nnoremap <A-down> :m .+1<CR>
autocmd VimEnter * vnoremap <A-up> :m '<-2<CR>gv=gv
autocmd VimEnter * vnoremap <A-down> :m '>+1<CR>gv=gv

"Map change buffer
autocmd VimEnter * nnoremap <A-left> :bprevious!<CR>
autocmd VimEnter * nnoremap <A-right> :bnext!<CR>

"Configure Startify
let g:startify_lists = [
			\ { 'type': 'bookmarks', 'header': ['Bookmarks']},
			\ ]
let g:startify_bookmarks = [ 
			\'~/dotfiles/', 
			\ '~/development/bidwrangler-react-app/'
			\ ]
let g:startify_custom_header = []

"Set GitGutter colours
let g:gitgutter_enabled = 1
let g:gitgutter_sign_column_always = 1
"hi GitGutterDelete guibg=#FF0F0F guifg=#FF0F0F
"hi GitGutterChange guibg=#FAEA41 guifg=#FAEA41
"hi GitGutterChangeDelete guibg=#FAEA41 guifg=#FAEA41
"hi GitGutterAdd guibg=#6AD945 guifg=#6AD945
"Toggle between GitGutter and Line Numbers
"map <leader>l :GitGutterToggle<CR>

"Tmux key binding
let g:tmux_navigator_no_mappings = 1
nnoremap <silent> <S-left> :TmuxNavigateLeft<cr>
nnoremap <silent> <S-down> :TmuxNavigateDown<cr>
nnoremap <silent> <S-up> :TmuxNavigateUp<cr>
nnoremap <silent> <S-right> :TmuxNavigateRight<cr>

" Centralize backups, swapfiles and undo history
set backupdir=/Users/ian/.vim/backups
set directory=/Users/ian/.vim/swaps
set undodir=/Users/ian/.vim/undo
