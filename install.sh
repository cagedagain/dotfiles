#!/bin/bash

bold=`tput bold`
normal=`tput sgr0`

# Select environment to set up
while true; do
  echo -n "${bold}What environment are you setting up?${normal} ([p]Personal [w]Work):"
  read environment
  case $environment in
    "p")
      branch=master
      ;;
    "w")
      branch=work
      ;;
  esac
  break
done

# Checkout chosen branch
git checkout ${branch}
git pull

# Create symlinks to config files (and create required directories)
ln -sf ~/dotfiles/tmux.conf ~/.tmux.conf
mkdir ~/.vim/colors ~/.vim/backups ~/.vim/swaps ~/.vim/undo
ln -sf ~/dotfiles/twilight.vim ~/.vim/colors/twilight.vim
ln -sf ~/dotfiles/gvimrc ~/.gvimrc
ln -sf ~/dotfiles/vimrc ~/.vimrc
ln -sf ~/dotfiles/zshrc ~/.zshrc
ln -sf ~/dotfiles/atom/config.cson ~/.atom/config.cson
ln -sf ~/dotfiles/atom/keymap.cson ~/.atom/keymap.cson
ln -sf ~/dotfiles/atom/package.list ~/.atom/package.list
ln -sf ~/dotfiles/atom/projects.cson ~/.atom/projects.cson
ln -sf ~/dotfiles/atom/snippets.cson ~/.atom/snippets.cson
ln -sf ~/dotfiles/atom/styles.less ~/.atom/styles.less

# Install atom packages
apm install --packages-file ~/.atom/package.list
