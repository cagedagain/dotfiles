## Files to be symlinked
`~/Library/Application Support/Code/User`
- `keybindings.json`
- `settings.json`
- `globalStorage/`
- `snippets/`


## Extensions to Install
- `extensionsList`

Export installed extensions to `extensionsList` file with `code --list-extensions | xargs -L 1 echo code --install-extension > ~/dotfiles/vscode/extensionsList`